# trying-less

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/trying-less/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/trying-less?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/trying-less?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/trying-less?style=for-the-badge)

[![repl](https://replit.com/badge/github/KennyOliver/trying-less)](https://replit.com/@KennyOliver/trying-less)

**Trying Less in Replit**

---
Kenny Oliver ©2021

[![GitHub Pages](https://img.shields.io/badge/See%20Demo-252525?style=for-the-badge&logo=safari&logoColor=white&link=https://kennyoliver.github.io/trying-less)](https://kennyoliver.github.io/trying-less)
